package tp4;

public class Pokemon {
	private int energie;
	private int maxEnergie;
	private String nom;
	//Atributs 
	//Constructeurs
	public Pokemon(String nom) {
		this.nom= nom;
		energie = 30 + (int)(Math.random() * (( maxEnergie - 30 ) + 1));
		maxEnergie = 50 + (int)(Math.random() * ((90 - 50) + 1));
	}
	
	
	
	//Accenseurs
	public String getNom() {
		return nom;
	}
	public int getEnergie(){
		return energie;
	}
	//M�thode
	//exo1.3
	public void sePresenter() {
		System.out.println("Je suis "   + nom + " j'ai " + energie + " points d'energie " + "(" + maxEnergie + " max"+")");
	}
	//exo1.5
	public void manger() {
			
			int repas = 10 + (int)(Math.random() * ((30 - 10) + 1));
			int energie = repas + this.energie;
			if (energie <= maxEnergie) 
					this.energie = energie;
			else
					this.energie= maxEnergie;
					
	}
	//exo1.6
	public void vivre() {
		int energie = 20 + (int)(Math.random() * ((40 - 20) + 1));
		int vie = this.energie - energie;
		if (vie >= 0)
			this.energie = energie;
		else
			this.energie = 0;
	}//exo1.7
	public boolean isAlive() {
			if(energie > 0)
			    return true;
			else
				return false;
				
				
	}
}
