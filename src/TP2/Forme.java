package tp2;

public class Forme {

	//Attributs
	private String couleur;
	private boolean coloriage;
	
	//Constructeur
	public Forme(){
		couleur="orange";
		coloriage=true;
	}
	public Forme (String c, boolean r){
		this.couleur=c;
		this.coloriage=r;
		
	}
	
	// Ascenseurs
	public String getCouleur(){
		return this.couleur;
	}
	public void setCouleur(String c){
		this.couleur=c;
	}
	public boolean isColoriage(){
		return this.coloriage;
	}
	public void setColoriage(boolean b){
		 this.coloriage=b;
	}
	
	//Méthode
	
    public String seDecrire() {
		return "une Forme de couleur " + couleur + " et de coloriage " + coloriage;
	}
	
	
}
