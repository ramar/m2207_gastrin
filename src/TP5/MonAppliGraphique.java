package TP5;

import  javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.BorderLayout; 

public class MonAppliGraphique extends JFrame {
	//Attributs
	//JButton a = new JButton("Hello Word"); 
	Container panneau = getContentPane(); 
	
	//exo2.3
	JButton a = new JButton("bouton 0"); 
	JButton b = new JButton("bouton 1"); 
	JButton c = new JButton("bouton 2"); 
	JButton d = new JButton("bouton 3"); 
	JButton e = new JButton("bouton 4"); 
	//JLabel b1;
	//JTextField c1; 
	 
	//Constructeurs

	public MonAppliGraphique() {
		super();
		this.setTitle("TestLayout"); 
		this.setSize(400,200);
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		
		//exo1.2
		System.out.println("La Fen�tre est cr��e");
		panneau.setLayout(new FlowLayout()); 
		panneau.add(a); 
		panneau.add(b); 
		panneau.add(c); 
		panneau.add(d); 
		panneau.add(e); 
		/*b1= new JLabel("Je suis un JLabel");
		panneau.add(b1);
		c1= new JTextField("Je suis un JTextField");
		panneau.add(c1);*/
		//exo 3.2 on divise la fen�tre en 5 zones
		panneau.setLayout(new GridLayout(3,2)) ; //3 lignes, 2 colonnes 
		panneau.add(a, BorderLayout.NORTH); 
		panneau.add(b, BorderLayout.SOUTH); 
		panneau.add(c, BorderLayout.EAST); 
		panneau.add(d, BorderLayout.WEST); 
		panneau.add(e, BorderLayout.CENTER); 
		 
		
		
		this.setVisible(true);// Toujours � la fin du constructeur}
		
	}
	//M�thodes

	//M�thodes Main
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique () ; 
	}
}
