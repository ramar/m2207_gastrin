package tp1;

public class Compte {
	
	//Attribut
	
	private int numero;
	private double decouvert;
	private double solde;
	
 //Constructeur
	public Compte (int numero){
		this.numero=numero;
		this.solde=0;
		this.decouvert=100;
		
	}
	
	// Accesseurs
	public double getDecouvert() {
	return this.decouvert;
	}
	public void setDecouvert(double montant) {
	this.decouvert = montant;
	}
	public int getNumero() {
	return this.numero;
	}
	public double getSolde() {
	return this.solde;
	}
	// Méthodes
	public void afficherSolde() {
	System.out.println("Votre solde est de : " + this.solde);
	
	}
	public void depot(double montant) {
		solde =  solde + montant;
		
	}
}
