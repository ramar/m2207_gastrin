package tp3;

public class Dame extends Humain {
	//Attributs
	protected boolean libre;
	
	//Constructeur exo2.2
	public Dame (String nom) {
		super(nom);
		boisson="Martini";
		libre=false;
	}
//M�thode
	public void priseEnOtage() {
		this.libre=true;
		parler("Au secours!");
	}
	
	public void estLiberee() {
		this.libre=false;
		parler("Merci Cowboys");
	}
}