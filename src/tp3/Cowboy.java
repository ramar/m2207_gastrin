package tp3;

public class Cowboy  extends Humain{
	// Attributs
		protected int popularite;
		protected String adjectif;
		
		// Constructeurs
		public Cowboy(String nom) {
			super(nom);
			boisson = "whiskey";
			popularite = 0;
			adjectif = "vaillant";
		}
		
		
}
