package tp3;

public class Brigand extends Humain {
 //Attributs exo3.1
	protected String look; 
	protected int nombreDameKidnappee, recompense; 
	protected boolean prison; 
	//Constructeurs exo3.2
	public Brigand (String nom ) {
		super(nom); 
		look = "m�chant"; 
		prison = false; 
		recompense = 100; 
		nombreDameKidnappee = 0; 
		boisson = "Cognac"; 
	}
	//Ascenseurs exo3.3
	public int getRecompense() { 
		return recompense;
	}
	
}
