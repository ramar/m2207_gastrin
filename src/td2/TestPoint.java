package td2;

public class TestPoint {

	public static void main(String[] args) {
		
		Point p;
		p = new Point(7,2);
		
		System.out.println ( "x=" + p.getX()); 
		System.out.println ( "y=" + p.getY()); 
		
		p.setX(5); // on met p. quand on veut intéragir avec l'objet qui est p ici
		System.out.println ( "x=" + p.getX()); 
		
		//Excercie 1.4
		//System.out.println ( "Exercice 1.4"); 
		//System.out.println ( "x=" + p.x); 
		
		//Exercice 1.8
		p.deplacer(-3, 2);
		System.out.println ( "Exercice 1.8"); 
		System.out.println ( "x=" + p.getX()); 
		System.out.println ( "y=" + p.getY()); 
		
		
	}

}
