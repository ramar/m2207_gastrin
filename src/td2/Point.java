package td2;

public class Point {
	//Attribut 
	private int x,y;
	
	//Constructeur
	public Point(int x1, int y1) {
		x = x1;
		y = y1;
				
	}
	//Accesseurs ->une m�thode simple pour lire la valeur d'un attribut 
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public void setX(int x) {
		this.x=x; 
	}
	
	//M�thodes
	public void deplacer(int a, int b) {
		x = x + a; 
		y = y + b;
		
	}
	
}
